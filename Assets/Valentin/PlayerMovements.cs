﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    public float moveHorizontal;
    public float speed = 10f;
    public float rayDistance = 2.5f;

    public float jumpForce = 10f;
    public float fallMultiplier = 1f;
    public float lowJumpMultiplier = 2f;

    public bool isFliped = false;

    public Rigidbody2D rgbd;
    public bool isGrounded = false;
    public LayerMask layers;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Récupère l'axe de 0 à 1 de la pression du bouton ou du joystick
        moveHorizontal = Input.GetAxis("Horizontal");
        //Multiplication par le temps pour que le joueur se déplace à une vitesse normale
        moveHorizontal *= Time.deltaTime;

        //Fait bouger le personnage dans l'espace avec une vitesse donnée, si il va a gauche
        //change la direction du sprite et inversement.
        //Modifie aussi la valeur isFliped pour gérer la direction de l'épée
        if (moveHorizontal < 0)
        {
            isFliped = true;
            transform.eulerAngles = new Vector3(0f, -180f, 0f);
            transform.Translate(-moveHorizontal * speed, 0, 0);
        }
        else if (moveHorizontal > 0)
        {
            isFliped = false;
            transform.eulerAngles = new Vector3(0f, 0f, 0f);
            transform.Translate(moveHorizontal * speed, 0, 0);
        }

        if (Physics2D.Raycast(transform.position, Vector2.down, rayDistance, layers))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }
}
